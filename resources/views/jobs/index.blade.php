@extends('layouts.master')

@section('jobs')

<div class="col-lg-6">
    <h2>Jobs</h2>
    <div class="table-responsive">
        <table class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Title</th>
                <th>Email</th>
                <th>Description</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($jobs as $job)
            <tr>
                <td>{{ $job->title }}</td>
                <td>{{ $job->email }}</td>
                <td>{{ $job->description }}</td>
                <td>
                    {{ $job->status() }}
                </td>


            </tr>
            @endforeach
            </tbody>
        </table>

        {{ $jobs->links() }}
    </div>
</div>
@endsection