@extends('layouts.master')

@section('jobs')
<h1>Create Job</h1>
<div class="row">

    <div class="col-lg-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        {!! Form::open(['route'=>'job.store','method'=>'post']) !!}

        <div class="form-group">
            <label>Title</label>
            {!! Form::text('title', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Email</label>
            {!! Form::text('email', null, ['class'=>'form-control']) !!}
        </div>

        <div class="form-group">
            <label>Description</label>
            {!! Form::textarea('description', null, ['class'=>'form-control']) !!}
        </div>
        <button class="btn btn-default" type="submit">Add</button>
        {!! Form::close() !!}
    </div>
</div>

@endsection