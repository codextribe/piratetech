<h1>New Job offer</h1>

<h3>{{ $job->title }}</h3>
<p>{{ $job->description }}</p>
<hr />
<a target="_blank" href="{{ route('job.publish', $job->id) }}">Publish</a> | <a target="_blank" href="{{ route('job.spam', $job->id) }}">Spam</a>