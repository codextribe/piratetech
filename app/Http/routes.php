<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('jobs', [
    'as'=>'job.index',
    'uses'=>'JobController@index'
]);

Route::get('{job}/publish', [
    'as'=>'job.publish',
    'uses'=>'JobController@publish'
]);

Route::get('{job}/spam', [
    'as'=>'job.spam',
    'uses'=>'JobController@spam'
]);

Route::get('job/create', [
    'as'=>'job.create',
    'uses'=>'JobController@create'
]);

Route::post('job/store', [
    'as'=>'job.store',
    'uses'=>'JobController@store'
]);
