<?php


namespace App\Http\Controllers;



use App\Models\HrManager;
use App\Models\Job;
use App\Http\Requests\Jobs\CreateRequest;
use App\Models\Moderator;
use Mail;

class JobController extends Controller
{

    public function index(Job $jobs)
    {
        $jobs = $jobs->paginate(5);

        return view('jobs.index', compact('jobs'));
    }

    public function create()
    {
        return view('jobs.create');
    }

    public function store(CreateRequest $request, Job $job, HrManager $manager, Moderator $moderator)
    {
        $job = $job->create($request->all());
        
        //Ovo bi trebalo da budu podaci iz tabele users
        $manager->name = 'Hr Manager';
        $manager->from = 'moderator@test.com';
        $manager->sendMail($job);

        $moderator->name = 'Moderator';
        $moderator->email = 'vladacicevac@gmail.com';
        $moderator->from = $job->email;
        $moderator->sendMail($job);

        return redirect()->to('jobs');
    }

    public function spam(Job $job)
    {
        $job->setAsSpam();

        return redirect()->to('jobs');
    }

    public function publish(Job $job)
    {
        $job->setAsPublic();

        return redirect()->to('jobs');
    }
}