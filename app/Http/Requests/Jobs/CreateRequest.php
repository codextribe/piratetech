<?php
namespace App\Http\Requests\Jobs;
/**
 * Created by IntelliJ IDEA.
 * User: vlada
 * Date: 12.7.16.
 * Time: 17.31
 */

use App\Http\Requests\Request;

class CreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required|max:255',
            'email' =>'required|email|unique:jobs',
            'description' =>'required',
        ];
    }

    /**
     * Set Custom Validation error messages
     *
     * @return array
     */
    public function messages() {
        return [
            'email.unique' => 'You are already posted job offer.'
        ];
    }

}