<?php
namespace App\Models;


use \Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
         'title', 'email', 'description'
    ];
    

    public function setAsPublic()
    {
        $this->status = config('const.publish');
        $this->save();
    }

    public function setAsSpam()
    {
        $this->status = config('const.spam');
        $this->save();
    }

    public function status()
    {
        switch($this->status){
            case config('const.publish');
                return 'Published';
            brake;
            case config('const.spam');
                return 'Spam';
        }
    }
}