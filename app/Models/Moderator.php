<?php

namespace App\Models;


use App\User;
use Mail;

class Moderator extends User
{
    use SendMailTrait;

    protected $emailTempate = 'emails.emailToModerator';

}