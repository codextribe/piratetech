<?php

namespace App\Models;
use Mail;

trait SendMailTrait
{
    public function sendMail($job)
    {
        Mail::send($this->emailTempate, ['job' => $job], function ($m) use ($job) {
            $m->from($this->from, 'Pirate technologies');

            $m->to($job->email, $this->name)->subject('Job offer');
        });
    }
}