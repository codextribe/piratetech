<?php


namespace App\Models;


use App\User;

class HrManager extends User
{
    use SendMailTrait;

    protected $emailTempate = 'emails.emailToHrManager';
}